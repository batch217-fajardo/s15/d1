console.log("Mabuhay");


// Js - is a loosely type Programming Language

alert("Hello Again");
console. log (  "Hello World" ) ;


console.
log
(
	"Hello World"

	); 

// [SECTION] - Making Comments in JS

/*

	1. Single-Line comment - Ctrl + /
	2. Multi-Line comment - Ctrl + Shift + /

*/

// [SECTION] - VARIABLES
// used to contain data.
// usually stored in a computer's memory.

// Declaration of variable

// Syntax --> let variableName;
let myVariable;

console.log(myVariable);

// Syntax --> let + variableName variableValue
let mySecondVariable = 2;

console.log(mySecondVariable);

let productName = "desktop computer";
console.log(productName);

// Reassigning value

let friend;

friend = "Kate";
friend = "Jane";
friend = "Erica";
console.log(friend);

// Syntax const variableName variableValue


// Reassigning value - const
const pet = "Bruno";
// pet = "Lala"
console.log(pet);

// const hoursPerDay = 24;


// Global and Local Variable

let outerVariable = "hello";  

{
	let innerVariable = "Hello World!";
	console.log(innerVariable);
}

console.log(outerVariable);

const outerExample = "Global Variable";

{
	const innerExample = "Local Variable"; 
	console.log(innerExample);
}

console.log(outerExample);

// Multiple Declaration

let productCode = "DC017", productBrand = "Deli";

/*let productCode = "DC017";
let productBrand = "Deli";*/

console.log(productCode, productBrand);

// [SECTION] Data Type

// Concatenating Strings
let country = 'Philippines';
let province = "Metro Manila";

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in" + ", " + country;
console.log(greeting);

// Escape character (/)
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early";
message ='John\'s employees went home early.';
message = "John\'s employees went \"home early\".";
console.log(message);

// Numbers
let headcount = 26;
console.log(headcount);

// Decimal Numbers or Fractions
let grade = 98.7;
console.log(grade);

// Exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining strings and integers/numbers
console.log("My grade last sem is " + grade);

// Boolean Data Type
let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Arrays Data Type

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Array w/ different Data Types
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
let person = {
	fullName: "Juan Dela Cruz", 
	age: "35",
	isMarried: false,
	contacts: ["09123456789" , "09999999999"],
	address: { 
		houseNumber:"345",
		city: "Manila",

	}
}

console.log(person);

// Reassigning value to an array
const anime = ["one piece", " one punch man", " attack on titan"];
anime[1] = ["Kimetsu no Yaiba"];

console.log(anime);

